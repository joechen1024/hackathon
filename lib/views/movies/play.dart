import 'package:desktop_webview_window/desktop_webview_window.dart';
import 'package:flutter/material.dart';

// import 'package:video_player/video_player.dart';
import 'dart:io';
import 'package:double_tap_player_view/double_tap_player_view.dart';
import 'package:video_player/video_player.dart';

/// @author: chenxiaoxi
/// @date: 2022/1/27
/// @description：play.dart

class MPlay extends StatefulWidget {
  const MPlay({Key? key}) : super(key: key);

  @override
  _MPlayState createState() => _MPlayState();
}

class _MPlayState extends State<MPlay> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    // 'http://www.sample-videos.com/video123/mp4/720/big_buck_bunny_720p_20mb.mp4')
//       ..initialize().then((_) => setState(() {}));
//     //http://mirror.aarnet.edu.au/pub/TED-talks/911Mothers_2010W-480p.mp4
//     //http://192.168.1.41:27018/resource/vod/%E8%A6%81%E5%81%9A%E4%B8%80%E4%B8%AA%E7%A7%AF%E6%9E%81%E5%90%91%E4%B8%8A%E7%9A%84%E4%BA%BA.mp4
    _controller = VideoPlayerController.network(
        'http://mirror.aarnet.edu.au/pub/TED-talks/911Mothers_2010W-480p.mp4')
      ..initialize().then((_) => setState(() {}));
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
          home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: AspectRatio(
            aspectRatio: 16 / 9,
            child: DoubleTapPlayerView(
              doubleTapConfig: DoubleTapConfig.create(),
              swipeConfig: SwipeConfig.create(overlayBuilder: _overlay),
              child: VideoPlayer(_controller),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _controller.play();
          },
          child: Icon(
            _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
          ),
        ),
      ));

  @override
  void afterFirstLayout(BuildContext context) => _controller.play();

  Widget _overlay(SwipeData data) {
    final dxDiff = (data.currentDx - data.startDx).toInt();
    Duration diffDuration = Duration(seconds: dxDiff);
    final prefix = diffDuration.isNegative ? '-' : '+';
    final positionText = '${prefix}${diffDuration.printDuration()}';
    final aimedDuration = diffDuration + Duration(minutes: 5);
    final diffText = aimedDuration.printDuration();

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            positionText,
            style: const TextStyle(
              fontSize: 30,
              color: Colors.white,
            ),
          ),
          SizedBox(height: 4),
          Text(
            diffText,
            style: const TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}

extension on Duration {
  /// ref: https://stackoverflow.com/a/54775297/8183034
  String printDuration() {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(inMinutes.abs().remainder(60));
    String twoDigitSeconds = twoDigits(inSeconds.abs().remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }
}
